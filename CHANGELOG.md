
## 0.0.11 [02-03-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/remove-element-from-array-by-value!8

---

## 0.0.10 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/remove-element-from-array-by-value!7

---

## 0.0.9 [06-06-2022]

* patch/DSUP-1361

See merge request itentialopensource/pre-built-automations/remove-element-from-array-by-value!6

---

## 0.0.8 [01-05-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/remove-element-from-array-by-value!5

---

## 0.0.7 [07-01-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/remove-element-from-array-by-value!4

---

## 0.0.6 [05-13-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/remove-element-from-array-by-value!3

---

## 0.0.5 [12-23-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/remove-element-from-array-by-value!2

---

## 0.0.4 [10-22-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/remove-element-from-array-by-value!2

---

## 0.0.3 [10-21-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/remove-element-from-array-by-value!1

---

## 0.0.2 [10-16-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
